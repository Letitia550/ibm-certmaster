package com.ibm.certmaster.controllers;

import com.ibm.certmaster.results.CertificationResult;
import com.ibm.certmaster.services.CertificationsServices;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CertificationsController {
    @CrossOrigin("*")
    @GetMapping("/certifications")
    public ArrayList<CertificationResult> getCertifications() {
        return CertificationsServices.getCertifications();
    }
}
