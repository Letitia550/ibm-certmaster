package com.ibm.certmaster.services;

import com.ibm.certmaster.exceptions.RequestParamValidationException;
import com.ibm.certmaster.models.Request;
import com.ibm.certmaster.results.RequestResult;
import com.ibm.certmaster.results.RequestResultExcel;
import com.ibm.certmaster.results.RequestResultPdf;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class RequestsServices {
    public static ArrayList<RequestResult> getRequestsOfUser(long userId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        String hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                "WHERE R.user_id = :userId\n";

        Query<RequestResult> query = session.createQuery(hql);
        query.setParameter("userId", userId);
        ArrayList<RequestResult> requests = (ArrayList<RequestResult>) query.list();

        session.getTransaction().commit();
        session.close();

        return requests;
    }

    public static RequestResult findRequestResult(long requestId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        String hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                "WHERE R.id = :requestId\n";

        Query query = session.createQuery(hql);
        query.setParameter("requestId", requestId);

        RequestResult request;

        try {
            request = (RequestResult) query.getSingleResult();
        } catch (NoResultException ex) {
            session.getTransaction().rollback();
            session.close();

            return null;
        }

        session.getTransaction().commit();
        session.close();

        return request;
    }

    public static Request findRequest(long requestId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        String hql = "FROM Request WHERE id = :id";

        Query query = session.createQuery(hql);
        query.setParameter("id", requestId);

        Request request = (Request) query.uniqueResult();

        session.getTransaction().commit();
        session.close();

        return request;
    }

    public static Request createRequest(Request request) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        request.setStatus("Pending");
        long insertedRequestId = (long) session.save(request);

        session.getTransaction().commit();
        session.close();

        return RequestsServices.findRequest(insertedRequestId);
    }

    public static void deleteRequest(Long requestId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        String hql = "DELETE FROM Request R WHERE R.id = :requestId";
        Query query = session.createQuery(hql);
        query.setParameter("requestId", requestId);
        query.executeUpdate();

        session.getTransaction().commit();
        session.close();
    }

    public static ArrayList<RequestResult> getAllRequest(Integer quarter, String status) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        String hql = "";
        ArrayList<RequestResult> requests = new ArrayList<>();

        if( quarter == null && status != null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                "INNER JOIN User U ON R.user_id = U.id\n" +
                "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                "WHERE R.status = :status ";

            Query query = session.createQuery(hql);
            query.setParameter("status", status);
            requests = (ArrayList<RequestResult>)  query.list();
        }

        if( quarter != null && status == null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                "INNER JOIN User U ON R.user_id = U.id\n" +
                "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                "WHERE C.quarter = :quarter";

            Query query = session.createQuery(hql);
            query.setParameter("quarter", quarter);
            requests = (ArrayList<RequestResult>)  query.list();
        }

        if( quarter == null && status == null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                "INNER JOIN User U ON R.user_id = U.id\n" +
                "INNER JOIN Category Ca ON C.category_id = Ca.id";

            Query query = session.createQuery(hql);
            requests = (ArrayList<RequestResult>)  query.list();
        }

        if( quarter != null && status != null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                "INNER JOIN User U ON R.user_id = U.id\n" +
                "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                "WHERE C.quarter = :quarter AND R.status = :status ";

            Query query = session.createQuery(hql);
            query.setParameter("quarter", quarter);
            query.setParameter("status", status);
            requests = (ArrayList<RequestResult>)  query.list();
        }

        session.getTransaction().commit();
        session.close();

        return requests;
    }

    public static ArrayList<RequestResultExcel> getAllRequestForExcel(Integer quarter, String status) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        String hql = "";
        ArrayList<RequestResultExcel> requests = new ArrayList<>();

        if( quarter == null && status != null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultExcel(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                    "WHERE R.status = :status ";

            Query query = session.createQuery(hql);
            query.setParameter("status", status);
            requests = (ArrayList<RequestResultExcel>)  query.list();
        }

        if( quarter != null && status == null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultExcel(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                    "WHERE C.quarter = :quarter";

            Query query = session.createQuery(hql);
            query.setParameter("quarter", quarter);
            requests = (ArrayList<RequestResultExcel>)  query.list();
        }

        if( quarter == null && status == null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultExcel(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id";

            Query query = session.createQuery(hql);
            requests = (ArrayList<RequestResultExcel>)  query.list();
        }

        if( quarter != null && status != null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultExcel(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                    "WHERE C.quarter = :quarter AND R.status = :status ";

            Query query = session.createQuery(hql);
            query.setParameter("quarter", quarter);
            query.setParameter("status", status);
            requests = (ArrayList<RequestResultExcel>)  query.list();
        }

        session.getTransaction().commit();
        session.close();

        return requests;
    }

    public static ArrayList<RequestResult> getAllRequest(int quarter, long userId, String status) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        String hql = "SELECT new com.ibm.certmaster.results.RequestResult(R.id, U.id, U.name, C.title, Ca.id, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                "FROM Request R\n" +
                "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                "INNER JOIN User U ON R.user_id = U.id\n" +
                "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                "WHERE C.quarter = :quarter  AND R.user_id = :userId AND R.status = :status";

        Query query = session.createQuery(hql);
        query.setParameter("quarter", quarter);
        query.setParameter("userId", userId);
        query.setParameter("status", status);

        ArrayList<RequestResult> requests = (ArrayList<RequestResult>) query.list();

        session.getTransaction().commit();
        session.close();

        return requests;
    }

    public static RequestResult updateRequest(String businessJustification, long requestId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        String hql = "UPDATE Request set business_justification = :businessJustification WHERE id = :requestId";

        Query query = session.createQuery(hql);
        query.setParameter("businessJustification", businessJustification);
        query.setParameter("requestId", requestId);
        query.executeUpdate();

        session.getTransaction().commit();
        session.close();

        return RequestsServices.findRequestResult(requestId);
    }

    public static RequestResult editRequest(String status, long requestId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        String hql = "UPDATE Request set status = :status WHERE id = :requestId";

        Query query = session.createQuery(hql);
        query.setParameter("status", status);
        query.setParameter("requestId", requestId);
        query.executeUpdate();

        session.getTransaction().commit();
        session.close();

        return RequestsServices.findRequestResult(requestId);

    }

    public static ArrayList<RequestResult> massAproveRequests(int quarter, long userId) {
        ArrayList<RequestResult> requestsToBeUpdated = getAllRequest(quarter, userId, "Pending");

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        String hql = "UPDATE Request R set R.status = 'Approved'\n " +
                "WHERE R.user_id = :userId\n" +
                    "AND R.certification_id IN (SELECT C.id FROM Certification  C WHERE quarter = :quarter)\n" +
                    "AND R.status = 'Pending'";

        Query query = session.createQuery(hql);
        query.setParameter("quarter", quarter);
        query.setParameter("userId", userId);
        query.executeUpdate();

        session.getTransaction().commit();
        session.close();

        return requestsToBeUpdated;
    }

    public static ArrayList<RequestResultPdf> getAllRequestForPDF(Integer quarter, String status){
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        String hql = "";
        ArrayList<RequestResultPdf> requests = new ArrayList<>();

        if( quarter == null && status != null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultPdf(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                    "WHERE R.status = :status ";

            Query query = session.createQuery(hql);
            query.setParameter("status", status);
            requests = (ArrayList<RequestResultPdf>)  query.list();
        }

        if( quarter != null && status == null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultPdf(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                    "WHERE C.quarter = :quarter";

            Query query = session.createQuery(hql);
            query.setParameter("quarter", quarter);
            requests = (ArrayList<RequestResultPdf>)  query.list();
        }

        if( quarter == null && status == null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultPdf(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id";

            Query query = session.createQuery(hql);
            requests = (ArrayList<RequestResultPdf>)  query.list();
        }

        if( quarter != null && status != null ) {
            hql = "SELECT new com.ibm.certmaster.results.RequestResultPdf(U.name, C.title, C.quarter, Ca.name, R.status, C.price, R.business_justification)\n" +
                    "FROM Request R\n" +
                    "INNER JOIN Certification C ON R.certification_id = C.id\n" +
                    "INNER JOIN User U ON R.user_id = U.id\n" +
                    "INNER JOIN Category Ca ON C.category_id = Ca.id\n" +
                    "WHERE C.quarter = :quarter AND R.status = :status ";

            Query query = session.createQuery(hql);
            query.setParameter("quarter", quarter);
            query.setParameter("status", status);
            requests = (ArrayList<RequestResultPdf>)  query.list();
        }

        session.getTransaction().commit();
        session.close();
        return requests;
    }

    public static PdfPTable createtable(RequestResultPdf i, int j){

        PdfPTable row = new PdfPTable(7);
        row.setKeepTogether(true);
        row.setWidthPercentage(100);
        row.addCell(String.valueOf(j));
        row.addCell(String.valueOf(i.getUserName()));
        row.addCell(String.valueOf(i.getCertificationTitle()));
        row.addCell(String.valueOf(i.getQuarter()));
        row.addCell(String.valueOf(i.getCategoryName()));
        row.addCell(String.valueOf(i.getStatus()));
        row.addCell(String.valueOf(i.getPrice()));

        return row;
    }

    public static byte[] createdocument(Integer quarter, String status) throws RequestParamValidationException, DocumentException {

        if( quarter != null ) {
            if (quarter < 1 || quarter > 4) {
                throw new RequestParamValidationException("Quarter must be 1, 2, 3 or 4.");
            }
        }

        if( status != null ) {
            if ( !status.equals("Approved") && !status.equals("Pending") && !status.equals("Rejected")) {
                throw new RequestParamValidationException("Status must be 'Approved', 'Pending' or 'Rejected'.");
            }
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Document document = new Document( PageSize.A4, 20, 20, 20, 20 );

        PdfWriter.getInstance(document, out);

        document.open();
        document.addTitle("Requests PDF");

        Paragraph title = new Paragraph();
        title.setAlignment(Element.ALIGN_CENTER);
        title.setFont(FontFactory.getFont(FontFactory.TIMES_BOLD,20,BaseColor.BLACK));

        if (quarter == null && status == null) {
            title.add("All Certification Requests");
            title.add("\n\n\n");
        }else if(quarter!=null && status==null){
            title.add("All Certification Requests by Quarter "+quarter);
            title.add("\n\n\n");
        }else if(quarter==null && status!=null){
            title.add("All Certification Requests which are "+status);
            title.add("\n\n\n");
        }else{
            title.add("All Certification Requests by Quarter "+quarter+" which are "+status);
            title.add("\n\n\n");
        }
        document.add(title);

        ArrayList<RequestResultPdf> dataset = RequestsServices.getAllRequestForPDF(quarter, status);

        PdfPTable headerRow = new PdfPTable(7);
        headerRow.setWidthPercentage(100);
        headerRow.setKeepTogether(true);
        headerRow.addCell("Number");
        headerRow.addCell("User");
        headerRow.addCell("Certification");
        headerRow.addCell("Quarter");
        headerRow.addCell("Category");
        headerRow.addCell("Status");
        headerRow.addCell("Price");

        document.add(headerRow);
        int j=0;
        for (RequestResultPdf i : dataset){
            j++;
            document.add(createtable(i,j));
            document.add(new Phrase(i.getBusinessJustification()));
        }

        document.close();

        byte[] pdfBytes = out.toByteArray();
        return pdfBytes;

    }
}
